package fromFileToDB;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import countries.Country;

public class ReadCSV {
	ReadFile file = new ReadFile();
	private List<Integer> deleteList = new ArrayList<>();
	
	public List<Country> csvData(String content) throws Exception {

	List<Country> countryList = new ArrayList<>();
	Country country = new Country();
	List<String> lineList = new ArrayList<>();
	List<String> fieldList = new ArrayList<>();
	List<String> columnList = new ArrayList<>();
	
	String delimiter = ";";
	String parameter;
	
	BufferedReader reader = new BufferedReader(new StringReader(content));
	String fileLine = reader.readLine();
	while (fileLine != null) {
		 lineList.add(fileLine);
		 fileLine = reader.readLine();
     }
	StringTokenizer columnNames = new StringTokenizer(lineList.get(0), delimiter);
	while(columnNames.hasMoreTokens()) {
		columnList.add(columnNames.nextToken());
	}
	
	for(int i = 1; i< lineList.size(); i++) {

		StringTokenizer st = new StringTokenizer(lineList.get(i), delimiter);
		while(st.hasMoreTokens()) {
			fieldList.add(st.nextToken());
		}
		
		for(int j = 0; j < columnList.size(); j++) {
			parameter =(String) columnList.get(j);
			
			switch(parameter) {
			case "ID":
				country.setId(Integer.valueOf((String) fieldList.get(j)));
				break;
				
			case "Name":
				country.setName((String) fieldList.get(j));
				break;
				
			case "CapitalCity":
				country.setCapital((String)fieldList.get(j));
				break;
				
			case "Currency":
				country.setCurrency((String)fieldList.get(j));
				break;
				
			case "GDP":
				country.setGdp(Double.valueOf((String)fieldList.get(j)));
				break;
				
			case "Delete":
				if(fieldList.size()<columnList.size()) {
					break;
				} else 
				if(fieldList.get(j).equalsIgnoreCase("x")) {
				deleteList.add(country.getId());
				}
				break;
		}
		}
			countryList.add(country);
			fieldList.clear();
			country = new Country();
		}
	return cleanList(countryList, deleteList);
	
}
	
	private List<Country> cleanList(List<Country> countryList, List<Integer> deleteList){
		List<Country> cleanList = new ArrayList<>();
		for(int i = 0; i< deleteList.size(); i++) {
			for(int j =0;j<countryList.size(); j++) {
				if(countryList.get(j).getId() != deleteList.get(i)) {
						cleanList.add(countryList.get(j));
				}
			}
		}
		return cleanList;
	}
	
	public List<Integer> getToDelete() {
		return this.deleteList;
	}
}