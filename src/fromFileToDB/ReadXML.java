package fromFileToDB;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import countries.Country;

public class ReadXML {
	
	List<Integer> deleteCountries = new ArrayList<Integer>();
	
	public List<Country> readXml (String content) {
		
		List<Country> listOfCountries = new ArrayList<Country>();
		
		File file = new File(content);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document xmlDoc = builder.parse(file);
			NodeList countryList = xmlDoc.getElementsByTagName("Country");
			for (int i = 0; i  < countryList.getLength(); i++) {
				Node c = countryList.item(i);
				if(c.getNodeType() ==  Node.ELEMENT_NODE) {
					Element country = (Element) c;
					int id = Integer.parseInt(country.getAttribute("ID"));
					
					if (country.getElementsByTagName("Delete").item(0) != null && country.getElementsByTagName("Delete").item(0).getTextContent().equalsIgnoreCase("X")) {
						deleteCountries.add(id);
					}
					
					String name = country.getElementsByTagName("Name").item(0).getTextContent();
					String capitalCity = country.getElementsByTagName("CapitalCity").item(0).getTextContent();
					String currency = country.getElementsByTagName("Currency").item(0).getTextContent();
					double gdp = Double.parseDouble(country.getElementsByTagName("GDP").item(0).getTextContent());

					
					
					Country cntry = new Country(id, name, capitalCity, currency, gdp);
									
					
					listOfCountries.add(cntry);
					
					
				}
		
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listOfCountries;
	}
	
	public List<Integer> deleteThese(){
		return deleteCountries;
	}
}
		