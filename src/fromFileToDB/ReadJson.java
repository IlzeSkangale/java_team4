package fromFileToDB;

import java.util.ArrayList;
import java.util.List;

import org.json.*;

import countries.Country;

public class ReadJson {
	
	private ArrayList<Integer> toDelete;
	
	public List<Country> readFile(String content) throws Exception {
		List<Country> countries = new ArrayList<Country>();
		toDelete = new ArrayList<Integer>();
		Country a;
		JSONObject obj = new JSONObject(content);
		JSONArray arr = obj.getJSONArray("Countries");

		for (int i = 0; i < arr.length(); i++) {
			if ((arr.getJSONObject(i).getString("Delete")).equalsIgnoreCase("x")) {
				toDelete.add(arr.getJSONObject(i).getInt("@ID"));
				continue;
			}
			int id = arr.getJSONObject(i).getInt("@ID");
			String name = arr.getJSONObject(i).getString("Name");
			String capital = arr.getJSONObject(i).getString("CapitalCity");
			String currency = arr.getJSONObject(i).getString("Currency");
			String tmp = arr.getJSONObject(i).getString("GDP");
			double gdp = Double.parseDouble(tmp);
			a = new Country(id, name, capital, currency, gdp);
			countries.add(a);
		}
		return countries;
	}
	
	public List<Integer> getToDelete(){
		return toDelete;
	}
}
