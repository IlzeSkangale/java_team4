package fromFileToDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import countries.Country;

public class UpdateDatabase {

	protected Connection con;

	public UpdateDatabase() throws Exception {
		con = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/project_database", "root", "");
		con.setAutoCommit(false);
	}

	public void process(List<Country> countries) {
		List<Integer> existingID = new ArrayList<>();
		List<Country> countriesToUpdate = new ArrayList<>();
		List<Country> countriesToInsert = new ArrayList<>();
		
		try {
			Statement stmt = con.createStatement();
			String sql = "SELECT ID FROM countries";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				existingID.add(rs.getInt("ID"));
			}
			
			if (existingID.isEmpty()) {
				this.insert(countries);
				return;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < countries.size(); i++) {
			int count = 0;
			for (int j = 0; j < existingID.size(); j++) {
				if (countries.get(i).getId() == (int) existingID.get(j)) {
					countriesToUpdate.add(countries.get(i));
					count++;
				}
			}
			
			if (count == 0) {
				countriesToInsert.add(countries.get(i));
			}
		}

		try {
			this.insert(countriesToInsert);
			this.update(countriesToUpdate);
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void insert(List<Country> countries) throws SQLException {
		if (countries == null)
			System.out.println("Nothing to insert");
		try {
			String sql = "INSERT IGNORE INTO countries (id, country, capitalCity, currency, gdp) VALUES (?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			for (int i = 0; i < countries.size(); i++) {
				Country tmp = countries.get(i);
				ps.setInt(1, tmp.getId());
				ps.setString(2, tmp.getName());
				ps.setString(3, tmp.getCapital());
				ps.setString(4, tmp.getCurrency());
				ps.setDouble(5, tmp.getGdp());
				ps.addBatch();
			}
			if (ps.executeBatch() != null) {
				con.commit();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void update(List<Country> countries) throws SQLException {
		try {
			String sql = "UPDATE countries SET country= ? , capitalCity= ? , currency= ? , gdp= ? WHERE id = ? ";
			PreparedStatement ps = con.prepareStatement(sql);
			Iterator<Country> iterator = countries.iterator();
			while (iterator.hasNext()) {
				Country tmp = iterator.next();
				ps.setInt(5, tmp.getId());
				ps.setString(1, tmp.getName());
				ps.setString(2, tmp.getCapital());
				ps.setString(3, tmp.getCurrency());
				ps.setDouble(4, tmp.getGdp());
				ps.addBatch();
			}
			if (ps.executeBatch() != null) {
				con.commit();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delete(List<Integer> delete) throws SQLException {
		try {
			String sql = "DELETE FROM countries WHERE id = ? ";
			PreparedStatement ps = con.prepareStatement(sql);

			Iterator<Integer> iterator = delete.iterator();
			while (iterator.hasNext()) {
				int tmp = iterator.next();
				ps.setInt(1, tmp);
				ps.addBatch();
			}
			if (ps.executeBatch() != null) {
				con.commit();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
