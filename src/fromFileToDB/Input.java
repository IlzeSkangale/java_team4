package fromFileToDB;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Input {

	private String filePath;
	private String fileExt;
	
//	public static void main(String[] args) {
//		Input a = new Input();
//		a.process();
//	}

	private String getFromConsole() throws IOException {
		System.out.println("Enter filepath: ");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		return reader.readLine();
	}

	public void process() {
		try {
			this.filePath = this.getFromConsole();
			this.getFileExtension(filePath);
	//		System.out.println(filePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getFileExtension(String filePath) throws IncorectFileExtension {
		StringBuffer tmp = new StringBuffer("");
		for (int i = (filePath.length() - 1); i > 0; i--) {
			if (filePath.charAt(i) == '.') {
				break;
			}
			tmp.append(filePath.charAt(i));
		}

		fileExt = tmp.reverse().toString();
		if (checkFileExtension(fileExt)) {
			return fileExt;
		} else {
			System.out.print("Incorrect filetype provided");
			return null;
//			throw new IncorectFileExtension();
		}
	}

	private boolean checkFileExtension(String ext) {
		if (!(ext.equalsIgnoreCase("xml")) && !(ext.equalsIgnoreCase("csv")) && !(ext.equalsIgnoreCase("json"))) {
			return false;
		}
		return true;
	}
	
	public String getFilePath() {
		return this.filePath;
	}
	
	public String getFileExtension() {
		return this.fileExt;
	}
}

class IncorectFileExtension extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IncorectFileExtension() {
		System.out.println("Incorrect file type provided");
	}
}
