package countries;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import fromDBtoFile.*;
import fromFileToDB.*;

public class Run {
	
public static void main(String[] args) throws Exception {
	
	System.out.println("What would you like to do? Type the number of the option: \n"
			+ "     1: Update the database from a file \n"
			+ "     2: Select data from the database and write to a file");
	
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	String input = reader.readLine();
	if(input.equalsIgnoreCase("1")) {
		update();		
	} else if (input.equalsIgnoreCase("2")) {
		create();
	} else System.out.println("Selection not valid");
}

	public static void update() throws Exception {
	Input input = new Input();
	input.process();
	
	ReadFile readFile = new ReadFile();
	readFile.setFile(input.getFilePath());
	
	List<Country> content = new ArrayList<Country>();
	
	UpdateDatabase update = new UpdateDatabase();
	
	switch (input.getFileExtension()) {
	case "csv":
		ReadCSV csv = new ReadCSV();
		content = csv.csvData(readFile.getContent());
		update.delete(csv.getToDelete());
		update.process(content);
		System.out.println("Database updated");
		break;
		
	case "json":
        ReadJson json = new ReadJson();
        content = json.readFile(readFile.getContent());
        update.delete(json.getToDelete());
        update.process(content);
        System.out.println("Database updated");
        break;
		
	case "xml":
		ReadXML xml = new ReadXML();
		content = xml.readXml(input.getFilePath());
		update.delete(xml.deleteThese());
		update.process(content);
		System.out.println("Database updated");
		break;
		}
	}
	
	public static void create() throws Exception {
	CreateFile create = new CreateFile();
	create.createFile();
	}
}

