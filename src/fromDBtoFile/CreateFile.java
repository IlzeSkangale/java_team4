package fromDBtoFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import fromFileToDB.*;

public class CreateFile extends Input{
	
	private String filePath;
	private String fileExt;
	
	public CreateFile() {	
	}
	
	public void createFile() throws Exception {
		this.process();
		GetFromDB get = new GetFromDB();
		if(this.fileExt.equalsIgnoreCase("xml")) {
			XMLfile.createXML(get.getFromDatabase(), filePath);
			System.out.println("XML file created");
		} else if (fileExt.equalsIgnoreCase("csv")) {
			CSVfile.createCsv(get.getFromDatabase(), filePath);
			System.out.println("CSV file created");
		} else if (fileExt.equalsIgnoreCase("json")) {
			JSONfile.createJson(get.getFromDatabase(), filePath);
			System.out.println("JSON file created");
		}else {
			System.out.println("Only csv, json or xml type files can be created!");
		}
	}

	@Override
	public void process() {
		try {
			this.filePath = this.getFromConsole();
			this.getFileExtension(filePath);
	//		System.out.println(filePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getFilePath() {
		// TODO Auto-generated method stub
		return super.getFilePath();
	}
	
	private String getFromConsole() throws IOException {
		System.out.println("Enter filepath: ");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		return reader.readLine();
	}

	private String getFileExtension(String filePath) throws IncorectFileExtension {
		StringBuffer tmp = new StringBuffer("");
		for (int i = (filePath.length() - 1); i > 0; i--) {
			if (filePath.charAt(i) == '.') {
				break;
			}
			tmp.append(filePath.charAt(i));
		}

		fileExt = tmp.reverse().toString();
		if (checkFileExtension(fileExt)) {
			return fileExt;
		} else {
			throw new IncorectFileExtension();
		}
	}

	private boolean checkFileExtension(String ext) {
		if (!(ext.equalsIgnoreCase("xml")) && !(ext.equalsIgnoreCase("csv")) && !(ext.equalsIgnoreCase("json"))) {
			return false;
		}
		return true;
	}
	
	class IncorectFileExtension extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public IncorectFileExtension() {
			System.err.println("Incorrect file type provided");
		}
	}
}
