package fromDBtoFile;

	import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
	import java.util.ArrayList;
	import java.util.List;

	import countries.Country;

	public class GetFromDB {

		protected Connection con;
		
		public String getSQL() throws IOException {
			System.out.println("Input field number to be searched: 1 = ID, 2 = Country name, 3 = Capital city, 4 = Currency, 5 = GDP:");
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String fieldNumber = reader.readLine();
			String sql = null;
			switch (fieldNumber) {
			case "1":
				sql = "SELECT ID, Country, CapitalCity, Currency, GDP FROM countries WHERE ID = ?";
				break;
			case "2":
				sql = "SELECT ID, Country, CapitalCity, Currency, GDP FROM countries WHERE Country = ?";
				break;
			case "3":
				sql = "SELECT ID, Country, CapitalCity, Currency, GDP FROM countries WHERE CapitalCity = ?";
				break;
			case "4":
				sql = "SELECT ID, Country, CapitalCity, Currency, GDP FROM countries WHERE Currency = ?";
				break;
			case "5":
				sql = "SELECT ID, Country, CapitalCity, Currency, GDP FROM countries WHERE GDP = ?";
			break;
			default: terminate();
			}
			return sql;
		}
		
		public String getValue() throws IOException {
			System.out.println("Input value to be searched:");
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String value = reader.readLine();
			return value;
		}

		public GetFromDB() throws Exception {
			con = null;
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/project_database", "root", "");
			con.setAutoCommit(false);
		}

		public List<Country> getFromDatabase() throws SQLException {
			List<Country> countries = new ArrayList<>();
			
			try {
				PreparedStatement ps = con.prepareStatement(getSQL());
				ps.setString(1, getValue());
				
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()) {
					Country c = new Country(rs.getInt("ID"), rs.getString("Country"), rs.getString("CapitalCity"), rs.getString("Currency"),
							rs.getDouble("GDP"));
					countries.add(c);
				}
				if(countries.isEmpty()) {
					System.out.println("No entries in the database match the specified parameters. No file created");
					System.exit(0);
				}
					
			} catch (SQLException | IOException e) {
				
				e.printStackTrace();
			} finally {
				con.close();
			}
		//	System.out.println(countries.toString());
			return countries;
		}
		
		public static void terminate() {
			System.out.println("Incorrect selection, program terminated");
			System.exit(0);
		}
}
