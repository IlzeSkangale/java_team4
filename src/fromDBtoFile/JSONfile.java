package fromDBtoFile;

import java.io.FileWriter;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import countries.Country;




public class JSONfile {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void createJson(List<Country> countries, String filePath) {
		Map jsonCountry = new LinkedHashMap();
		JSONArray countriesArray = new JSONArray();
		for (int i = 0; i < countries.size(); i++) {
			jsonCountry.put("@ID", countries.get(i).getId());
			jsonCountry.put("Name", countries.get(i).getName());
			jsonCountry.put("CapitalCity", countries.get(i).getCapital());
			jsonCountry.put("Currency", countries.get(i).getCurrency());
			jsonCountry.put("GDP", countries.get(i).getGdp());
			countriesArray.add(jsonCountry);
		}
		
		JSONObject mainCountries = new JSONObject();
		mainCountries.put("Countries", countriesArray);
		
		try (FileWriter file = new FileWriter(filePath)){
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String prettyJson = gson.toJson(mainCountries);
			
			file.write(prettyJson);
			file.close();
		} catch (IOException e){
			e.printStackTrace();
		}	
	}
}
