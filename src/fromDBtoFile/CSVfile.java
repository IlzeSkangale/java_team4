package fromDBtoFile;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import countries.Country;

public class CSVfile {

	private static final String delimiter = ";";

	public static void createCsv(List<Country> objects, String filepath) throws Exception {

		FileOutputStream file = new FileOutputStream(filepath);
		PrintStream stream = new PrintStream(file);

		stream.println("ID" + delimiter + "Name" + delimiter + "CapitalCity" + delimiter + "Currency" + delimiter
				+ "GDP" + delimiter);

		Iterator<Country> iter = objects.iterator();
		while (iter.hasNext()) {
			Country tmp = iter.next();
			StringBuffer line = new StringBuffer();
			line.append(tmp.getId());
			line.append(delimiter);
			line.append(tmp.getName());
			line.append(delimiter);
			line.append(tmp.getCapital());
			line.append(delimiter);
			line.append(tmp.getCurrency());
			line.append(delimiter);
			line.append(tmp.getGdp());
			line.append(delimiter);
			stream.println(line.toString());
		}

		stream.close();
		file.close();
	}

}
