package fromDBtoFile;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import countries.Country;

public class XMLfile {

	public static void createXML(List<Country> countries, String filepath) throws Exception {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document document = docBuilder.newDocument();
		
		Element countriesElement = document.createElement("Countries");
		document.appendChild(countriesElement);
		
		Iterator<Country> iterator = countries.iterator();
		
		while(iterator.hasNext()) {
			Country c = iterator.next();
		
		Element countryElement = document.createElement("Country");
		countryElement.setAttribute("ID", String.valueOf(c.getId()));
		
		Element countryName = document.createElement("Name");
		countryName.setTextContent(c.getName());
		countryElement.appendChild(countryName);
		
		Element capitalCity = document.createElement("CapitalCity");
		capitalCity.setTextContent(c.getCapital());
		countryElement.appendChild(capitalCity);
		
		Element currency = document.createElement("Currency");
		currency.setTextContent(c.getCurrency());
		countryElement.appendChild(currency);
		
		Element gdp = document.createElement("GDP");
		gdp.setTextContent(String.valueOf(c.getGdp()));
		countryElement.appendChild(gdp);
		
		countriesElement.appendChild(countryElement);
	}
		DOMSource domSource = new DOMSource(document);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

		transformer.transform(domSource, result);
//		System.out.println(writer.toString());
		
		FileOutputStream file = new FileOutputStream(filepath); 
		PrintStream printStObj = new PrintStream(file);

		printStObj.print(writer.toString());
		printStObj.close();
		file.close();
}
}
